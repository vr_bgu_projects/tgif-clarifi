﻿using CsvHelper;
using FileHelpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using tgif_clarifi.Models;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace tgif_clarifi
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static string CLARIFI_API_ROOT = "https://api.clarifai.com/v1/";
        private static readonly long MIN_TTL_MS = 60000;
        private static string client_id = "hkOom3PZmyIrKsyyx8mYU7nJ5gM2BU_TT_N8h4jI";
        private static string client_secret = "Xan8IwoAc2qKqPve_T6HzW46y6AJlUM39rqhElVB";
        private static string tgif_file_path;
        private static ClarifaiAccessToken clarifaiAccessToken;
        private static List<Gif> gif_array;
        private static List<GifResult> GifResult_array;

        static void Main(string[] args)
        {
            try
            {
                if (args.Length == 0)
                {
                    Console.WriteLine("Empty tgif file as argument");
                    return;
                }
                ReadTgifFile(args[0]);
                //string token = RetrieveAccessToken();
                int countId = 1;
                if (args.Length > 1 && !String.IsNullOrEmpty(args[1]))
                    countId = Int32.Parse(args[1]) + 1;

                gif_array = GetNRandomElements(100, gif_array);

                SetClarifaiAccesstoken();
                GifResult_array = new List<GifResult>();
                foreach (Gif gif in gif_array)
                {
                    //if (countId != 1 && gif.GifId < countId)
                    //{
                    //    continue;
                    //}
                    Console.WriteLine(Environment.NewLine + "Runing for image #id = " + gif.GifId);
                    //GifResult gifRes = RunTagForUrl(gif);

                    
                    GifResult gifRes = RunTagForUrlCurl(gif);

                    //string tagsOutput = "";
                    //foreach (Tag tag in gifRes.tagsList)
                    //{
                    //    tagsOutput = tagsOutput + tag.ToString() + ",";
                    //}
                    //if (tagsOutput.Length > 2)
                    //    tagsOutput.Remove(tagsOutput.Length - 2);

                    //// This text is always added, making the file longer over time
                    //// if it is not deleted.
                    //using (StreamWriter sw = File.AppendText("tgif_clarifi_results.csv"))
                    //{
                    //    sw.WriteLine(countId + ", \"" + gifRes.gif.GifUrl + "\", " + EscapeCSV(gifRes.gif.GifDesc) + ", " + EscapeCSV(tagsOutput) +" ,"+ gifRes.time);
                    //}

                    GifResult_array.Add(gifRes);
                    countId++;
                }

                string results = Newtonsoft.Json.JsonConvert.SerializeObject(GifResult_array);
                File.WriteAllText("tgif_clarifi_result_100_randoms", results);

            }
            catch (Exception ex)
            {
                ReportError(ex);
            }
            
        }

        public static string EscapeCSV(string data)
        {
            if (data.Contains("\""))
            {
                data = data.Replace("\"", "\"\"");
            }

            if (data.Contains(","))
            {
                data = String.Format("\"{0}\"", data);
            }

            if (data.Contains(System.Environment.NewLine))
            {
                data = String.Format("\"{0}\"", data);
            }

            return data;
        }

        static List<Gif> ReadTgifFile(string filePath)
        {
            try
            {
                tgif_file_path = filePath;
                TextReader reader = File.OpenText(tgif_file_path);
                var csv = new CsvReader(reader);
                csv.Configuration.Delimiter = "|";
                csv.Configuration.HasHeaderRecord = false;
                gif_array = csv.GetRecords<Gif>().ToList();                

                return gif_array;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                log.Error(ex);
            }
            return null;
        }

        //static string RetrieveAccessToken()
        //{
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(Program.access_token) && System.Environment.TickCount < Program.expiration - MIN_TTL_MS)
        //            return Program.access_token;

        //        var client = new RestClient(CLARIFI_API_ROOT);                
        //        var request = new RestRequest("token", Method.POST);

        //        request.AddParameter("grant_type", "client_credentials");
        //        request.AddParameter("client_id", client_id);
        //        request.AddParameter("client_secret",client_secret);

        //        request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                
        //        // execute the request
        //        IRestResponse response = client.Execute(request);
        //        var content = response.Content; // raw content as string
        //        dynamic json = JValue.Parse(content);
        //        // values require casting

        //        if (response.StatusCode !=  System.Net.HttpStatusCode.OK)
        //        {
        //            log.Error(content);
        //        }
        //        access_token = json.access_token;
        //        int expiresIn = json.expires_in;
        //        expiration = System.Environment.TickCount + expiresIn;

        //        return access_token;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex);
        //    }
        //    return "";
        //}

        ///// <summary>
        ///// Launch the legacy application with some options set.
        ///// </summary>
        //static GifResult RunTagForUrl(Gif gif)
        //{
        //    GifResult gifRes = new GifResult();
        //    try
        //    {
        //        gifRes.gif = gif;
        //        var proc = new Process
        //        {
        //            StartInfo = new ProcessStartInfo
        //            {
        //                FileName = "Run.bat",
        //                Arguments = gif.GifUrl,
        //                UseShellExecute = false,
        //                RedirectStandardOutput = true,
        //                CreateNoWindow = true
        //            }
        //        };
        //        // Create new stopwatch.
        //        Stopwatch stopwatch = new Stopwatch();

        //        // Begin timing.
        //        stopwatch.Start();
        //        proc.Start();
        //        stopwatch.Stop();
        //        gifRes.time = stopwatch.Elapsed.TotalMilliseconds + " ms";

        //        List<Tag> tagsList = new List<Tag>();
        //        while (!proc.StandardOutput.EndOfStream)
        //        {
        //            string line = proc.StandardOutput.ReadLine();
        //            Console.WriteLine(line);
        //            if (line.Contains("("))
        //            {
        //                string[] tagResults = line.Replace(")", "").Split('(');
        //                Tag tag = new Tag(tagResults[0].Trim(), Double.Parse(tagResults[1].Trim()));
        //                tagsList.Add(tag);
        //            }
        //        }

        //        gifRes.tagsList = tagsList;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //        log.Error(ex);
        //    }

        //    return gifRes;
        //}


        static GifResult RunTagForUrlCurl(Gif gif)
        {
            GifResult gifRes = new GifResult();
            try
            {
                gifRes.gif = gif;
                
                // Create new stopwatch.
                Stopwatch stopwatch = new Stopwatch();

                // Begin timing.
                stopwatch.Start();
                string tagArguments = string.Format("\"{0}tag/?url={1}\" -H \"Authorization: Bearer {2}\"",CLARIFI_API_ROOT,gif.GifUrl,clarifaiAccessToken.access_token);
                string tagResults = RunProcess("curl.exe", tagArguments);
                stopwatch.Stop();
                gifRes.time = stopwatch.Elapsed.TotalMilliseconds + " ms";
                //dynamic stuff = JObject.Parse(tagResults);

                ClarifiTagResponse clarifiTagResponse = JsonConvert.DeserializeObject<ClarifiTagResponse>(tagResults);
                gifRes.ClarifiTagResponse = clarifiTagResponse;

                //List<Tag> tagsList = new List<Tag>();


                //gifRes.tagsList = tagsList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                log.Error(ex);
            }

            return gifRes;
        }
   
        static void SetClarifaiAccesstoken()
        {            
            try
            {
                string processFileName = "curl.exe";
                string arguments = string.Format("-X POST \"{0}token/\" -d \"client_id={1}\" -d \"client_secret={2}\" -d \"grant_type=client_credentials\"",CLARIFI_API_ROOT, client_id, client_secret);
                string result = RunProcess(processFileName,arguments);
                clarifaiAccessToken = JsonConvert.DeserializeObject<ClarifaiAccessToken>(result);
                
            }
            catch (Exception ex)
            {
                ReportError(ex);
            }
        }

        private static string RunProcess(string processFileName, string arguments)
        {
            StringBuilder result = new StringBuilder();

            try
            {
                var proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = processFileName,
                        Arguments = arguments,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };
                // Create new stopwatch.
                Stopwatch stopwatch = new Stopwatch();

                // Begin timing.
                stopwatch.Start();
                proc.Start();
                stopwatch.Stop();

                while (!proc.StandardOutput.EndOfStream)
                {
                    string line = proc.StandardOutput.ReadLine();
                    result.AppendLine(line);
                    Console.WriteLine(line);
                }
                Console.WriteLine(string.Format("Elapsed Time (ms): {0}", stopwatch.ElapsedMilliseconds));
            }
            catch (Exception ex)
            {
                ReportError(ex);
            }

            return result.ToString();
        }

        private static void ReportError(Exception ex)
        {
            string message = ex.ToString();
            log.Error(message);
            Console.WriteLine(message);
        }

        private static List<Gif> GetNRandomElements(int n, List<Gif> gifsList)
        {
            return gifsList.OrderBy(x => rnd.Next()).Take(n).ToList<Gif>();
        }


        //Function to get random number
        private static readonly Random rnd = new Random();
        private static readonly object syncLock = new object();
        public static int GetRandomNumber(int min, int max)
        {
            lock (syncLock)
            { // synchronize
                return rnd.Next(min, max);
            }
        }
    }



    public class ClarifaiAccessToken
    {
        public ClarifaiAccessToken()
        {
        }
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string scope { get; set; }
    }
}
